#!/usr/bin/env python

import sys,os,time
import paramiko
import argparse

# Check python version
def preparePython():
    if sys.version_info[0] < 3:
        print("\033[93mPython version 2 or lower is not supported\033[0m")
        exit(1)

# Color entry message
class textPaint:
    colors={"HEADER":"\033[95m",
            "OK":"\033[92m",
            "WARN":"\033[93m",
            "OKBLUE":"\033[94m",
            "ERR":"\033[91m",
            "BOLD":"\033[1m",
            "UNDERLINE":"\033[4m",
            "ENDC":"\033[0m"
            }
# Catch and forward textPaint class
def catchMessage(color,message):
    print(textPaint.colors[color] + message + textPaint.colors["ENDC"])

# Script arguments parser
def argsParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--ip', required=True, metavar='', help='Server hostname or IP address')
    parser.add_argument('-s','--sql', required=False, metavar='', help='Download SQL')
    parser.add_argument('-p','--password', required=False, metavar='', help='SSH password')
    parser.add_argument('-P','--port', required=False, metavar='', default='22', help='SSH port')
    parser.add_argument('-d','--dir', required=False, metavar='', default='~/public_html', help='Website remote root directory')
    parser.add_argument('-u','--username', required=True, metavar='', help='SSH username')
    parser.add_argument('-m','--media', required=False, metavar='', help='Download media')
# Humanize variables
    global ip,username,password,port,dir
    ip=parser.parse_args().ip
    username=parser.parse_args().username
    password=parser.parse_args().password
    port=parser.parse_args().port
    dir=parser.parse_args().dir

def selfUpdate():
    dqw

def main():
    preparePython()
    argsParser()
